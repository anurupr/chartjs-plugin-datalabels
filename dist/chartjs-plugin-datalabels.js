/*!
 * chartjs-plugin-datalabels v0.7.0
 * https://chartjs-plugin-datalabels.netlify.com
 * (c) 2019 Chart.js Contributors
 * Released under the MIT license
 */
(function (global, factory) {
typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('chart.js')) :
typeof define === 'function' && define.amd ? define(['chart.js'], factory) :
(global = global || self, global.ChartDataLabels = factory(global.Chart));
}(this, function (Chart) { 'use strict';

Chart = Chart && Chart.hasOwnProperty('default') ? Chart['default'] : Chart;

var helpers = Chart.helpers;

var devicePixelRatio = (function() {
	if (typeof window !== 'undefined') {
		if (window.devicePixelRatio) {
			return window.devicePixelRatio;
		}

		// devicePixelRatio is undefined on IE10
		// https://stackoverflow.com/a/20204180/8837887
		// https://github.com/chartjs/chartjs-plugin-datalabels/issues/85
		var screen = window.screen;
		if (screen) {
			return (screen.deviceXDPI || 1) / (screen.logicalXDPI || 1);
		}
	}

	return 1;
}());

var utils = {
	// @todo move this in Chart.helpers.toTextLines
	toTextLines: function(inputs) {
		var lines = [];
		var input;

		inputs = [].concat(inputs);
		while (inputs.length) {
			input = inputs.pop();
			if (typeof input === 'string') {
				lines.unshift.apply(lines, input.split('\n'));
			} else if (Array.isArray(input)) {
				inputs.push.apply(inputs, input);
			} else if (!helpers.isNullOrUndef(inputs)) {
				lines.unshift('' + input);
			}
		}

		return lines;
	},

	// @todo move this method in Chart.helpers.canvas.toFont (deprecates helpers.fontString)
	// @see https://developer.mozilla.org/en-US/docs/Web/CSS/font
	toFontString: function(font) {
		if (!font || helpers.isNullOrUndef(font.size) || helpers.isNullOrUndef(font.family)) {
			return null;
		}

		return (font.style ? font.style + ' ' : '')
			+ (font.weight ? font.weight + ' ' : '')
			+ font.size + 'px '
			+ font.family;
	},

	// @todo move this in Chart.helpers.canvas.textSize
	// @todo cache calls of measureText if font doesn't change?!
	textSize: function(ctx, lines, font) {
		var items = [].concat(lines);
		var ilen = items.length;
		var prev = ctx.font;
		var width = 0;
		var i;

		ctx.font = font.string;

		for (i = 0; i < ilen; ++i) {
			width = Math.max(ctx.measureText(items[i]).width, width);
		}

		ctx.font = prev;

		return {
			height: ilen * font.lineHeight,
			width: width
		};
	},

	// @todo move this method in Chart.helpers.options.toFont
	parseFont: function(value) {
		var global = Chart.defaults.global;
		var size = helpers.valueOrDefault(value.size, global.defaultFontSize);
		var font = {
			family: helpers.valueOrDefault(value.family, global.defaultFontFamily),
			lineHeight: helpers.options.toLineHeight(value.lineHeight, size),
			size: size,
			style: helpers.valueOrDefault(value.style, global.defaultFontStyle),
			weight: helpers.valueOrDefault(value.weight, null),
			string: ''
		};

		font.string = utils.toFontString(font);
		return font;
	},

	/**
	 * Returns value bounded by min and max. This is equivalent to max(min, min(value, max)).
	 * @todo move this method in Chart.helpers.bound
	 * https://doc.qt.io/qt-5/qtglobal.html#qBound
	 */
	bound: function(min, value, max) {
		return Math.max(min, Math.min(value, max));
	},

	/**
	 * Returns an array of pair [value, state] where state is:
	 * * -1: value is only in a0 (removed)
	 * *  1: value is only in a1 (added)
	 */
	arrayDiff: function(a0, a1) {
		var prev = a0.slice();
		var updates = [];
		var i, j, ilen, v;

		for (i = 0, ilen = a1.length; i < ilen; ++i) {
			v = a1[i];
			j = prev.indexOf(v);

			if (j === -1) {
				updates.push([v, 1]);
			} else {
				prev.splice(j, 1);
			}
		}

		for (i = 0, ilen = prev.length; i < ilen; ++i) {
			updates.push([prev[i], -1]);
		}

		return updates;
	},

	/**
	 * https://github.com/chartjs/chartjs-plugin-datalabels/issues/70
	 */
	rasterize: function(v) {
		return Math.round(v * devicePixelRatio) / devicePixelRatio;
	}
};

function orient(point, origin) {
	var x0 = origin.x;
	var y0 = origin.y;

	if (x0 === null) {
		return {x: 0, y: -1};
	}
	if (y0 === null) {
		return {x: 1, y: 0};
	}

	var dx = point.x - x0;
	var dy = point.y - y0;
	var ln = Math.sqrt(dx * dx + dy * dy);

	return {
		x: ln ? dx / ln : 0,
		y: ln ? dy / ln : -1
	};
}

function aligned(x, y, vx, vy, align) {
	switch (align) {
	case 'center':
		vx = vy = 0;
		break;
	case 'bottom':
		vx = 0;
		vy = 1;
		break;
	case 'right':
		vx = 1;
		vy = 0;
		break;
	case 'left':
		vx = -1;
		vy = 0;
		break;
	case 'top':
		vx = 0;
		vy = -1;
		break;
	case 'start':
		vx = -vx;
		vy = -vy;
		break;
	case 'end':
		// keep natural orientation
		break;
	default:
		// clockwise rotation (in degree)
		align *= (Math.PI / 180);
		vx = Math.cos(align);
		vy = Math.sin(align);
		break;
	}

	return {
		x: x,
		y: y,
		vx: vx,
		vy: vy
	};
}

// Line clipping (Cohen–Sutherland algorithm)
// https://en.wikipedia.org/wiki/Cohen–Sutherland_algorithm

var R_INSIDE = 0;
var R_LEFT = 1;
var R_RIGHT = 2;
var R_BOTTOM = 4;
var R_TOP = 8;

function region(x, y, rect) {
	var res = R_INSIDE;

	if (x < rect.left) {
		res |= R_LEFT;
	} else if (x > rect.right) {
		res |= R_RIGHT;
	}
	if (y < rect.top) {
		res |= R_TOP;
	} else if (y > rect.bottom) {
		res |= R_BOTTOM;
	}

	return res;
}

function clipped(segment, area) {
	var x0 = segment.x0;
	var y0 = segment.y0;
	var x1 = segment.x1;
	var y1 = segment.y1;
	var r0 = region(x0, y0, area);
	var r1 = region(x1, y1, area);
	var r, x, y;

	// eslint-disable-next-line no-constant-condition
	while (true) {
		if (!(r0 | r1) || (r0 & r1)) {
			// both points inside or on the same side: no clipping
			break;
		}

		// at least one point is outside
		r = r0 || r1;

		if (r & R_TOP) {
			x = x0 + (x1 - x0) * (area.top - y0) / (y1 - y0);
			y = area.top;
		} else if (r & R_BOTTOM) {
			x = x0 + (x1 - x0) * (area.bottom - y0) / (y1 - y0);
			y = area.bottom;
		} else if (r & R_RIGHT) {
			y = y0 + (y1 - y0) * (area.right - x0) / (x1 - x0);
			x = area.right;
		} else if (r & R_LEFT) {
			y = y0 + (y1 - y0) * (area.left - x0) / (x1 - x0);
			x = area.left;
		}

		if (r === r0) {
			x0 = x;
			y0 = y;
			r0 = region(x0, y0, area);
		} else {
			x1 = x;
			y1 = y;
			r1 = region(x1, y1, area);
		}
	}

	return {
		x0: x0,
		x1: x1,
		y0: y0,
		y1: y1
	};
}

function compute(range, config) {
	var anchor = config.anchor;
	var segment = range;
	var x, y;

	if (config.clamp) {
		segment = clipped(segment, config.area);
	}

	if (anchor === 'start') {
		x = segment.x0;
		y = segment.y0;
	} else if (anchor === 'end') {
		x = segment.x1;
		y = segment.y1;
	} else {
		x = (segment.x0 + segment.x1) / 2;
		y = (segment.y0 + segment.y1) / 2;
	}

	return aligned(x, y, range.vx, range.vy, config.align);
}

var positioners = {
	arc: function(vm, config) {
		var angle = (vm.startAngle + vm.endAngle) / 2;
		var vx = Math.cos(angle);
		var vy = Math.sin(angle);
		var r0 = vm.innerRadius;
		var r1 = vm.outerRadius;

		return compute({
			x0: vm.x + vx * r0,
			y0: vm.y + vy * r0,
			x1: vm.x + vx * r1,
			y1: vm.y + vy * r1,
			vx: vx,
			vy: vy
		}, config);
	},

	point: function(vm, config) {
		var v = orient(vm, config.origin);
		var rx = v.x * vm.radius;
		var ry = v.y * vm.radius;

		return compute({
			x0: vm.x - rx,
			y0: vm.y - ry,
			x1: vm.x + rx,
			y1: vm.y + ry,
			vx: v.x,
			vy: v.y
		}, config);
	},

	rect: function(vm, config) {
		var v = orient(vm, config.origin);
		var x = vm.x;
		var y = vm.y;
		var sx = 0;
		var sy = 0;

		if (vm.horizontal) {
			x = Math.min(vm.x, vm.base);
			sx = Math.abs(vm.base - vm.x);
		} else {
			y = Math.min(vm.y, vm.base);
			sy = Math.abs(vm.base - vm.y);
		}

		return compute({
			x0: x,
			y0: y + sy,
			x1: x + sx,
			y1: y,
			vx: v.x,
			vy: v.y
		}, config);
	},

	fallback: function(vm, config) {
		var v = orient(vm, config.origin);

		return compute({
			x0: vm.x,
			y0: vm.y,
			x1: vm.x,
			y1: vm.y,
			vx: v.x,
			vy: v.y
		}, config);
	}
};

var helpers$1 = Chart.helpers;
var rasterize = utils.rasterize;

function boundingRects(model) {
	var borderWidth = model.borderWidth || 0;
	var padding = model.padding;
	var th = model.size.height;
	var tw = model.size.width;
	var tx = -tw / 2;
	var ty = -th / 2;

	return {
		frame: {
			x: tx - padding.left - borderWidth,
			y: ty - padding.top - borderWidth,
			w: tw + padding.width + borderWidth * 2,
			h: th + padding.height + borderWidth * 2
		},
		text: {
			x: tx,
			y: ty,
			w: tw,
			h: th
		}
	};
}

function getScaleOrigin(el) {
	var horizontal = el._model.horizontal;
	var scale = el._scale || (horizontal && el._xScale) || el._yScale;

	if (!scale) {
		return null;
	}

	if (scale.xCenter !== undefined && scale.yCenter !== undefined) {
		return {x: scale.xCenter, y: scale.yCenter};
	}

	var pixel = scale.getBasePixel();
	return horizontal ?
		{x: pixel, y: null} :
		{x: null, y: pixel};
}

function getPositioner(el) {
	if (el instanceof Chart.elements.Arc) {
		return positioners.arc;
	}
	if (el instanceof Chart.elements.Point) {
		return positioners.point;
	}
	if (el instanceof Chart.elements.Rectangle) {
		return positioners.rect;
	}
	return positioners.fallback;
}

//https://math.stackexchange.com/questions/176310/formula-for-calculating-the-center-of-an-arc
function getCenterOfArc(xPos, yPos, sAngle, eAngle, radius) {
	let angleDiff = eAngle - sAngle;
	let angle = (sAngle + eAngle) / 2.0;
	let x = 0.0;
	let y = 0.0;
	let offset = 0.0;
	if(angleDiff < 0.0) {
		offset = Math.PI;
	}
	x = (xPos + Math.cos((angle + offset) * radius));
	y = (yPos + -Math.sin((angle + offset) * radius));
	return { x, y };
}

function getStartPointOfArc(arc) {
	const r_start_angle  = arc.startAngle + ( 2 * Math.PI )
	return {
		// x : circle_center_x + Math.cos(r_start_angle) * r_length;
		// y = circle_center_y + Math.sin(r_start_angle) * r_length;
		x: arc.x + Math.cos(r_start_angle) * arc.outerRadius,
		y: arc.y + Math.cos(r_start_angle) * arc.outerRadius
	}
}
function getEndPointOfArc(arc) {
	const r_end_angle  = arc.endAngle + (  2 * Math.PI)
	return {
		// x : circle_center_x + Math.cos(r_end_angle) * r_length;
		// y = circle_center_y + Math.sin(r_end_angle) * r_length;
		x: arc.x + Math.cos(r_end_angle) * arc.outerRadius,
		y: arc.y + Math.cos(r_end_angle) * arc.outerRadius
	}
}

function getMidPointOfArc(arc) {
	// const s = getPointOnArc(vmx.x, vmx.y, vmx.outerRadius, vmx.startAngle)
	// const e = getPointOnArc(vmx.x, vmx.y, vmx.outerRadius, vmx.endAngle)
	// let c=Math.atan(s.y + e.y, s.x + e.x);
	let c = (arc.startAngle + (arc.endAngle > arc.startAngle ? arc.endAngle : arc.endAngle + Math.PI*2)) * 0.5;
	return {
		x: arc.x + (arc.outerRadius * Math.cos(c)),
		y: arc.y + (arc.outerRadius * Math.sin(c))
	}
}

function getPointOnArc(cx, cy, radius, angle) {
    return {
		x: cx + Math.cos(angle) * radius,
		y: cy + Math.sin(angle) * radius
	};
}

function transformPoint(point, matrix) {
	return {
		x: matrix.a * point.x + matrix.c * point.y + matrix.e,
		y: matrix.b * point.x + matrix.d * point.y + matrix.f,
	}
}

function getOptimalLabelDockPoints(rect) {
	// left mid, top mid, right mid, bottom mid
	return [
		// left mid
		{
			x: rect.x,
			y: rect.y + (rect.h / 2)
		},
		// top mid
		{
			x: (rect.x + (rect.x + rect.w)) / 2,
			y: (rect.y - 2)
		},
		// right mid
		{
			x: rect.x + rect.w,
			y: rect.y + (rect.h / 2)
		},
		// bottom mid
		{
			x: (rect.x + (rect.x + rect.w)) / 2,
			y: (rect.y + rect.h) - 2
		}
	]
}

function getTransformedOptimalLabelDockPoints(optimalPoints, transform) {
	return optimalPoints.map(p => transformPoint(p, transform))
}

function getDistanceBetweenTwoPoints(p1, p2) {
	var a = p1.x - p2.x;
	var b = p1.y - p2.y;

	return Math.sqrt( a*a + b*b );
}

function getClosestLabelDockPoint(point, optimalPoints, transform) {
	if (optimalPoints.length > 0) {
		const transformedOptimalPoints = getTransformedOptimalLabelDockPoints(optimalPoints, transform)
		var distances = transformedOptimalPoints.map((o, i) => { return { i, d: getDistanceBetweenTwoPoints(point, o)} })
		distances.sort((p1, p2) => p1.d < p2.d ? -1 : 1)
		return optimalPoints[distances[0].i]
	} else {
		return point;
	}
}

// Ref: https://stackoverflow.com/a/37225895
function interceptCircleLineSeg(circle, line){
    var a, b, c, d, u1, u2, ret, retP1, retP2, v1, v2;
    v1 = {};
    v2 = {};
    v1.x = line.p2.x - line.p1.x;
    v1.y = line.p2.y - line.p1.y;
    v2.x = line.p1.x - circle.center.x;
    v2.y = line.p1.y - circle.center.y;
    b = (v1.x * v2.x + v1.y * v2.y);
    c = 2 * (v1.x * v1.x + v1.y * v1.y);
    b *= -2;
    d = Math.sqrt(b * b - 2 * c * (v2.x * v2.x + v2.y * v2.y - circle.radius * circle.radius));
    if(isNaN(d)){ // no intercept
        return [];
    }
    u1 = (b - d) / c;  // these represent the unit distance of point one and two on the line
    u2 = (b + d) / c;
    retP1 = {};   // return points
    retP2 = {}
    ret = []; // return array
    if(u1 <= 1 && u1 >= 0){  // add point if on the line segment
        retP1.x = line.p1.x + v1.x * u1;
        retP1.y = line.p1.y + v1.y * u1;
        ret[0] = retP1;
    }
    if(u2 <= 1 && u2 >= 0){  // second add point if on the line segment
        retP2.x = line.p1.x + v1.x * u2;
        retP2.y = line.p1.y + v1.y * u2;
        ret[ret.length] = retP2;
    }
    return ret;
}

function drawFrame(ctx, rect, model, chart, me, center, transform) {

	// console.log('center', center, chart, me)
	const curTransform = ctx.getTransform();

	// draw line
	// ctx.translate(rasterize(center.x), rasterize(center.y));
	// ctx.rotate(model.rotation);
	// console.log('drawFrame')
	// console.log('me', me._index, me._el, me._el._model.x,me._el._model.y)

	// // this will only work for pie chart and not doughnut , since we are only using outerRadius
	// // if this needs to be done for doughnut chart, then you would have to incorporate innerRadius as well
	let vmx = me._el._view;
	// console.log('vmx.x', vmx.x, 'vmx.y', vmx.y);
	const mpoa = getMidPointOfArc(vmx)

	const optimalPoints = getOptimalLabelDockPoints(rect);
	const lineStart = getClosestLabelDockPoint(mpoa, optimalPoints, curTransform);

	// console.log('lineStart', lineStart)

	ctx.beginPath();

	ctx.arc(rasterize(lineStart.x), rasterize(lineStart.y), 2, 0, 2 * Math.PI, false);
	ctx.fillStyle = 'white';
	ctx.fill();
	ctx.lineWidth = 1;
	ctx.strokeStyle = '#888'
	ctx.closePath();
	ctx.stroke();

	ctx.beginPath();
	ctx.moveTo(rasterize(lineStart.x), rasterize(lineStart.y))
	// ctx.moveTo(rasterize(intercept[0].x), rasterize(intercept[0].y))
	ctx.setTransform(transform)
	// let centerofArc = getCenterOfArc(vmx.x, vmx.y, vmx.startAngle, vmx.startAngle + (Math.PI * 2), vmx.outerRadius)
	// console.log(me._el.getCenterPoint(), centerofArc);
	// // ctx.lineTo(rasterize(rect.x - 25), rasterize(rect.y - 25))
	// console.log('coa.x', centerofArc.x, 'coa.y', centerofArc.y)
	ctx.lineTo(mpoa.x, mpoa.y)
	ctx.closePath();
	ctx.stroke();

	ctx.beginPath();
	ctx.arc(rasterize(mpoa.x), rasterize(mpoa.y), 2, 0, 2 * Math.PI, false);
	ctx.fillStyle = 'white';
	ctx.fill();
	ctx.closePath();
	ctx.stroke();


	ctx.setTransform(curTransform)

	var bgColor = model.backgroundColor;
	// let parsedColor = parseColor(bgColor);
	// bgColor = `rgba(${parsedColor[0]}, ${parsedColor[1]}, ${parsedColor[2]}, .6)`
	// console.log('bgColor', bgColor)
	var borderColor = model.borderColor;
	var borderWidth = model.borderWidth;

	if (!bgColor && (!borderColor || !borderWidth)) {
		return;
	}

	ctx.beginPath();

	helpers$1.canvas.roundedRect(
		ctx,
		rasterize(rect.x) + borderWidth / 2,
		rasterize(rect.y) + borderWidth / 2,
		rasterize(rect.w) - borderWidth,
		rasterize(rect.h) - borderWidth,
		model.borderRadius);

	ctx.closePath();

	if (bgColor) {
		ctx.fillStyle = bgColor;
		ctx.fill();
	}

	// if (borderColor && borderWidth) {
	// 	ctx.strokeStyle = borderColor;
	// 	ctx.lineWidth = borderWidth;
	// 	ctx.lineJoin = 'miter';
	// 	ctx.stroke();
	// }

	// let arc = me._el._model;
	// arc.endAngle = arc.startAngle + (Math.PI * 2)
	// const elView = me._el._view;
	// ctx.beginPath();
	// ctx.arc(arc.x - 200, arc.y, arc.outerRadius, arc.startAngle, arc.endAngle)
	// ctx.arc(arc.x - 200, arc.y, arc.innerRadius, arc.endAngle, arc.startAngle, true);
	// ctx.fillStyle = "#ccc";
	// ctx.strokeStyle = "#000";
	// ctx.fill();
	// ctx.stroke();


}

function textGeometry(rect, align, font) {
	var h = font.lineHeight;
	var w = rect.w;
	var x = rect.x;
	var y = rect.y + h / 2;

	if (align === 'center') {
		x += w / 2;
	} else if (align === 'end' || align === 'right') {
		x += w;
	}

	return {
		h: h,
		w: w,
		x: x,
		y: y
	};
}

function drawTextLine(ctx, text, cfg) {
	var shadow = ctx.shadowBlur;
	var stroked = cfg.stroked;
	var x = rasterize(cfg.x);
	var y = rasterize(cfg.y);
	var w = rasterize(cfg.w);

	if (stroked) {
		ctx.strokeText(text, x, y, w);
	}

	if (cfg.filled) {
		if (shadow && stroked) {
			// Prevent drawing shadow on both the text stroke and fill, so
			// if the text is stroked, remove the shadow for the text fill.
			ctx.shadowBlur = 0;
		}

		ctx.fillText(text, x, y, w);

		if (shadow && stroked) {
			ctx.shadowBlur = shadow;
		}
	}
}

function drawText(ctx, lines, rect, model) {
	var align = model.textAlign;
	var color = model.color;
	var filled = !!color;
	var font = model.font;
	var ilen = lines.length;
	var strokeColor = model.textStrokeColor;
	var strokeWidth = model.textStrokeWidth;
	var stroked = strokeColor && strokeWidth;
	var i;

	if (!ilen || (!filled && !stroked)) {
		return;
	}

	// Adjust coordinates based on text alignment and line height
	rect = textGeometry(rect, align, font);

	ctx.font = font.string;
	ctx.textAlign = align;
	ctx.textBaseline = 'middle';
	ctx.shadowBlur = model.textShadowBlur;
	ctx.shadowColor = model.textShadowColor;

	if (filled) {
		ctx.fillStyle = color;
	}
	if (stroked) {
		ctx.lineJoin = 'round';
		ctx.lineWidth = strokeWidth;
		ctx.strokeStyle = strokeColor;
	}

	for (i = 0, ilen = lines.length; i < ilen; ++i) {
		drawTextLine(ctx, lines[i], {
			stroked: stroked,
			filled: filled,
			w: rect.w,
			x: rect.x,
			y: rect.y + rect.h * i
		});
	}
}

var Label = function(config, ctx, el, index) {
	var me = this;

	me._config = config;
	me._index = index;
	me._model = null;
	me._rects = null;
	me._ctx = ctx;
	me._el = el;
};

helpers$1.extend(Label.prototype, {
	/**
	 * @private
	 */
	_modelize: function(display, lines, config, context) {
		var me = this;
		var index = me._index;
		var resolve = helpers$1.options.resolve;
		var font = utils.parseFont(resolve([config.font, {}], context, index));
		var color = resolve([config.color, Chart.defaults.global.defaultFontColor], context, index);

		return {
			align: resolve([config.align, 'center'], context, index),
			anchor: resolve([config.anchor, 'center'], context, index),
			area: context.chart.chartArea,
			backgroundColor: resolve([config.backgroundColor, null], context, index),
			borderColor: resolve([config.borderColor, null], context, index),
			borderRadius: resolve([config.borderRadius, 0], context, index),
			borderWidth: resolve([config.borderWidth, 0], context, index),
			clamp: resolve([config.clamp, false], context, index),
			clip: resolve([config.clip, false], context, index),
			color: color,
			display: display,
			font: font,
			lines: lines,
			offset: resolve([config.offset, 0], context, index),
			opacity: resolve([config.opacity, 1], context, index),
			origin: getScaleOrigin(me._el),
			padding: helpers$1.options.toPadding(resolve([config.padding, 0], context, index)),
			positioner: getPositioner(me._el),
			rotation: resolve([config.rotation, 0], context, index) * (Math.PI / 180),
			size: utils.textSize(me._ctx, lines, font),
			textAlign: resolve([config.textAlign, 'start'], context, index),
			textShadowBlur: resolve([config.textShadowBlur, 0], context, index),
			textShadowColor: resolve([config.textShadowColor, color], context, index),
			textStrokeColor: resolve([config.textStrokeColor, color], context, index),
			textStrokeWidth: resolve([config.textStrokeWidth, 0], context, index),
			fixLabelOverlap: resolve([config.fixLabelOverlap, false], context, index),

		};
	},

	update: function(context) {
		var me = this;
		var model = null;
		var rects = null;
		var index = me._index;
		var config = me._config;
		var value, label, lines;

		// We first resolve the display option (separately) to avoid computing
		// other options in case the label is hidden (i.e. display: false).
		var display = helpers$1.options.resolve([config.display, true], context, index);

		if (display) {
			value = context.dataset.data[index];
			label = helpers$1.valueOrDefault(helpers$1.callback(config.formatter, [value, context]), value);
			lines = helpers$1.isNullOrUndef(label) ? [] : utils.toTextLines(label);

			if (lines.length) {
				model = me._modelize(display, lines, config, context);
				rects = boundingRects(model);
			}
		}

		me._model = model;
		me._rects = rects;
	},

	geometry: function() {
		return this._rects ? this._rects.frame : {};
	},

	rotation: function() {
		return this._model ? this._model.rotation : 0;
	},

	visible: function() {
		return this._model && this._model.opacity;
	},

	model: function() {
		return this._model;
	},

	draw: function(chart, center) {
		var me = this;
		var ctx = chart.ctx;
		var model = me._model;
		var rects = me._rects;
		var area;


		if (!this.visible()) {
			return;
		}

		if (this._el._view.label == "Appium hub application") {
			// console.log('center draw', center)
		}



		ctx.save();

		if (model.clip) {
			area = model.area;
			ctx.beginPath();
			ctx.rect(
				area.left,
				area.top,
				area.right - area.left,
				area.bottom - area.top);
			ctx.clip();
		}

		ctx.globalAlpha = utils.bound(0, model.opacity, 1);
		const transform = ctx.getTransform();
		if (this.$layout.fixedCenter) {
			console.log('using fixedcenter', this.$layout.fixedCenter)
			ctx.translate(rasterize(this.$layout.fixedCenter.center.x), rasterize(this.$layout.fixedCenter.center.y));
		} else {
			ctx.translate(rasterize(center.x), rasterize(center.y));
		}
		ctx.rotate(model.rotation);
		drawFrame(ctx, rects.frame, model, chart, me, center, transform);
		drawText(ctx, model.lines, rects.text, model);

		ctx.restore();
	}
});

var helpers$2 = Chart.helpers;

var MIN_INTEGER = Number.MIN_SAFE_INTEGER || -9007199254740991; // eslint-disable-line es/no-number-minsafeinteger
var MAX_INTEGER = Number.MAX_SAFE_INTEGER || 9007199254740991;  // eslint-disable-line es/no-number-maxsafeinteger

function rotated(point, center, angle) {
	var cos = Math.cos(angle);
	var sin = Math.sin(angle);
	var cx = center.x;
	var cy = center.y;

	return {
		x: cx + cos * (point.x - cx) - sin * (point.y - cy),
		y: cy + sin * (point.x - cx) + cos * (point.y - cy)
	};
}

function projected(points, axis) {
	var min = MAX_INTEGER;
	var max = MIN_INTEGER;
	var origin = axis.origin;
	var i, pt, vx, vy, dp;

	for (i = 0; i < points.length; ++i) {
		pt = points[i];
		vx = pt.x - origin.x;
		vy = pt.y - origin.y;
		dp = axis.vx * vx + axis.vy * vy;
		min = Math.min(min, dp);
		max = Math.max(max, dp);
	}

	return {
		min: min,
		max: max
	};
}

function toAxis(p0, p1) {
	var vx = p1.x - p0.x;
	var vy = p1.y - p0.y;
	var ln = Math.sqrt(vx * vx + vy * vy);

	return {
		vx: (p1.x - p0.x) / ln,
		vy: (p1.y - p0.y) / ln,
		origin: p0,
		ln: ln
	};
}

var HitBox = function() {
	this._rotation = 0;
	this._rect = {
		x: 0,
		y: 0,
		w: 0,
		h: 0
	};
};

helpers$2.extend(HitBox.prototype, {
	center: function() {
		var r = this._rect;
		return {
			x: r.x + r.w / 2,
			y: r.y + r.h / 2
		};
	},

	update: function(center, rect, rotation) {
		this._rotation = rotation;
		this._rect = {
			x: rect.x + center.x,
			y: rect.y + center.y,
			w: rect.w,
			h: rect.h
		};
	},

	contains: function(point) {
		var me = this;
		var margin = 1;
		var rect = me._rect;

		point = rotated(point, me.center(), -me._rotation);

		return !(point.x < rect.x - margin
			|| point.y < rect.y - margin
			|| point.x > rect.x + rect.w + margin * 2
			|| point.y > rect.y + rect.h + margin * 2);
	},

	// Separating Axis Theorem
	// https://gamedevelopment.tutsplus.com/tutorials/collision-detection-using-the-separating-axis-theorem--gamedev-169
	intersects: function(other) {
		var r0 = this._points();
		var r1 = other._points();
		var axes = [
			toAxis(r0[0], r0[1]),
			toAxis(r0[0], r0[3])
		];
		var i, pr0, pr1;

		if (this._rotation !== other._rotation) {
			// Only separate with r1 axis if the rotation is different,
			// else it's enough to separate r0 and r1 with r0 axis only!
			axes.push(
				toAxis(r1[0], r1[1]),
				toAxis(r1[0], r1[3])
			);
		}

		for (i = 0; i < axes.length; ++i) {
			pr0 = projected(r0, axes[i]);
			pr1 = projected(r1, axes[i]);

			if (pr0.max < pr1.min || pr1.max < pr0.min) {
				return false;
			}
		}

		return true;
	},

	/**
	 * @private
	 */
	_points: function() {
		var me = this;
		var rect = me._rect;
		var angle = me._rotation;
		var center = me.center();

		return [
			rotated({x: rect.x, y: rect.y}, center, angle),
			rotated({x: rect.x + rect.w, y: rect.y}, center, angle),
			rotated({x: rect.x + rect.w, y: rect.y + rect.h}, center, angle),
			rotated({x: rect.x, y: rect.y + rect.h}, center, angle)
		];
	}
});

function coordinates(view, model, geometry) {
	var point = model.positioner(view, model);
	var vx = point.vx;
	var vy = point.vy;

	if (!vx && !vy) {
		// if aligned center, we don't want to offset the center point
		return {x: point.x, y: point.y};
	}

	var w = geometry.w;
	var h = geometry.h;

	// take in account the label rotation
	var rotation = model.rotation;
	var dx = Math.abs(w / 2 * Math.cos(rotation)) + Math.abs(h / 2 * Math.sin(rotation));
	var dy = Math.abs(w / 2 * Math.sin(rotation)) + Math.abs(h / 2 * Math.cos(rotation));

	// scale the unit vector (vx, vy) to get at least dx or dy equal to
	// w or h respectively (else we would calculate the distance to the
	// ellipse inscribed in the bounding rect)
	var vs = 1 / Math.max(Math.abs(vx), Math.abs(vy));
	dx *= vx * vs;
	dy *= vy * vs;

	// finally, include the explicit offset
	dx += model.offset * vx;
	dy += model.offset * vy;

	return {
		x: point.x + dx,
		y: point.y + dy
	};
}

// top-left and right, bottom-left and right
function getBoxOuterPoints(box) {
	const rect = box._box._rect
	return {
		tl: {
			x: rect.x,
			y: rect.y
		},
		tr: {
			x: rect.x + rect.w,
			y: rect.y
		},
		bl: {
			x: rect.x,
			y: rect.y + rect.h
		},
		br: {
			x: rect.x + rect.w,
			y: rect.y + rect.h
		}
	}
}

function getMidPoint(p1, p2) {
	return {
		x: (p1.x + p2.x) / 2,
		y: (p1.y + p2.y) / 2
	}
}

// function getDistanceBetweenTwoPoints(p1, p2) {
// 	var a = p1.x - p2.x;
// 	var b = p1.y - p2.y;

// 	return Math.sqrt( a*a + b*b );
// }

function getClosestPointQuartet(oPoints1, oPoints2) {
	// oPoints1 contains the outer points of the box we want to move
	// oPoints2 contains the outer points of the box we want to compare oPoints1 with
	// the mode is just for our reference. in reality , we just need the mid points of the closest point quartet
	// to move box1 to
	// set1 is left
	// set2 is right
	// get midpoint of points in set 1 mpSet1
	// get midpoint of points in set 2 mpSet2
	// move left of oPoints1 to mpSet1
	// move right of oPoints1 to mpSet2
	// update the rest of the points accordingly
	// TODO midpoint may not work for all cases, test it out and see if the algo works
	//

	// since its a box, we can check bl and br of first and tl and tr of second  OR vice-versa
	console.log('getClosestPointQuartet start');
	console.log('checking bl and br of 1 and tl and tr of 2')
	const dBL1TL2 = getDistanceBetweenTwoPoints(oPoints1.bl, oPoints2.tl);
	const dBR1TR2 = getDistanceBetweenTwoPoints(oPoints1.br, oPoints2.tr);
	console.log('dBL1TL2', dBL1TL2);
	console.log('dBR1TR2', dBR1TR2);
	console.log('checking tl and tr of 1 and bl and br of 2')
	const dTL1BL2 = getDistanceBetweenTwoPoints(oPoints1.tl, oPoints2.bl);
	const dTR1BR2 = getDistanceBetweenTwoPoints(oPoints1.tr, oPoints2.br);
	console.log('dTL1BL2', dTL1BL2);
	console.log('dTR1BR2', dTR1BR2);
	console.log('getClosestPointQuartet end');

	if (dBL1TL2 < dTL1BL2 && dBR1TR2 < dTR1BR2) {
		// MODE 1 , oPoints2 is below oPoints1
		return {
			quartet: {
				set1: {
					p1: oPoints1.bl,
					p2: oPoints2.tl
				},
				set2: {
					p1: oPoints1.br,
					p2: oPoints2.tr
				}
			},
			mode: 1
		}
	} else if ( (dTL1BL2 < dBL1TL2)  && (dTR1BR2 < dBR1TR2) ){
		// MODE 2, oPoints2 is above oPoints1
		return {
			quartet: {
				set1: {
					p1: oPoints1.tl,
					p2: oPoints2.bl
				},
				set2: {
					p1: oPoints1.tr,
					p2: oPoints2.br
				}
			},
			mode: 2
		}
	} else {
		console.log('mode 3', oPoints1, oPoints2)
	}

}

function getMidPointOfSet(set) {
	return getMidPoint(set.p1, set.p2)
}


function handleCollision(labels, colliding) {

	// handle collision here
	// get previous box of s0 and forward box of s1
	console.log(labels)
	// const previousBoxOfS0 = s0._idx > 0 ? labels[s0._idx - 1] : labels[labels.length - 1];
	// const forwardBoxOfS1 = s1._idx == labels.length - 1 ? labels[0] : labels[s1._idx + 1]
	let collidingHandled =
	colliding.map((c) => {
		let S0 = c.s0;
		let S1 = c.s1;
		const l0 = getLabelBasedOnIdx(S0._idx, labels)
		const l1 = getLabelBasedOnIdx(S1._idx, labels)
		if(l0.label._el._view.label == "Appium hub browser") {
			return;
		}
		console.log('S0'
		    , 'name', l0.label._el._view.label
			, 'tl', S0._opoints.tl
			, 'tr', S0._opoints.tr
			, 'bl', S0._opoints.bl
			, 'br', S0._opoints.br
		);

		console.log('S1'
			, 'name', l1.label._el._view.label
			, 'tl', S1._opoints.tl
			, 'tr', S1._opoints.tr
			, 'bl', S1._opoints.bl
			, 'br', S1._opoints.br
		);

		//PBS0
		let PBS0 = S0._idx > 0 ? getLabelBasedOnIdx(S0._idx - 1, labels) : getLabelBasedOnIdx(labels.length - 1, labels);
		const previousBoxOfS0 = PBS0.label
		//FBS1
		let FBS1 = S1._idx == labels.length - 1 ? getLabelBasedOnIdx(0, labels) : getLabelBasedOnIdx(S1._idx + 1, labels);
		const forwardBoxOfS1 = FBS1.label

		// const mPBS0 = getMidPoint(S0, prev)
		console.log('previousBoxOfS0'
			, 'name', previousBoxOfS0._el._view.label
			, 'tl', previousBoxOfS0.$layout._opoints.tl
			, 'tr', previousBoxOfS0.$layout._opoints.tr
			, 'bl', previousBoxOfS0.$layout._opoints.bl
			, 'br', previousBoxOfS0.$layout._opoints.br
		)

		console.log('forwardBoxOfS1'
			, 'name', forwardBoxOfS1._el._view.label
			, 'tl', forwardBoxOfS1.$layout._opoints.tl
			, 'tr', forwardBoxOfS1.$layout._opoints.tr
			, 'bl', forwardBoxOfS1.$layout._opoints.bl
			, 'br', forwardBoxOfS1.$layout._opoints.br
		)

		const closestPointQuartet = getClosestPointQuartet(S0._opoints, previousBoxOfS0.$layout._opoints)
		console.log('closestPointQuartet', closestPointQuartet)
		const mpSet1 = getMidPointOfSet(closestPointQuartet.quartet.set1)
		const mpSet2 = getMidPointOfSet(closestPointQuartet.quartet.set2)

		console.log('mpSet1', mpSet1);
		console.log('mpSet2', mpSet2);

		// we may not need set2
		let tl = {}
		tl.x = 0;
		tl.y = 0;

		// get rekt
		const rect = S0._box._rect;
		// x coord would be same
		tl.x = mpSet1.x;

		if (closestPointQuartet.mode == 1) {
			// if mode 1, then we need to calculate tl from bl using _rect.w and _rect.y
			tl.y = mpSet1.y + rect.h;

		} else if (closestPointQuartet.mode == 2){
			tl.x = mpSet1.x;
			tl.y = mpSet1.y;
		}

		console.log('tl', tl)
		const label = labels[l0.ix];
		// let's try updating the label from here
		console.log('l0.ix', l0.ix, label)
		console.log('label', labels[l0.ix].$layout._box._rect)

		// console.log('label', labels[l0.ix].$layout._box._rect)
		// labels[l0.ix].update(labels[l0.ix].$context);
		// console.log('this', labels[l0.ix])
		// labels[l0.ix].$context.chart[EXPANDO_KEY]._dirty = true;
		// invalidate(labels[l0.ix].$context.chart)

		// let geometry = label.geometry();
		// let center = coordinates(label._el._view, label.model(), geometry);
		let center = { x: ((tl.x + rect.w) / 2), y: tl.y + (rect.h / 2) }

		console.log('center', center)
		label.$layout.fixedCenter = {
			tl,
			rect,
			center
		}
		// label.$layout._box.update(center, geometry, label.rotation());
		// // label._rects.frame.x = 0;
		// // label._rects.frame.y = 0;
		// label.draw(label.$context.chart, center);
		// invalidate(label.$context.chart)
		return {
			s0: S0,
			s1: S1
		}
	});
}

function convertToNamed(opoints) {
	return {
		tl: opoints[0],
		tr: opoints[1],
		br: opoints[2],
		bl: opoints[3]
	}
}

function collide(labels, collider) {
	// changes by anurupr
	// slight modification
	// returning colliding boxes

	var i, j, s0, s1, colliding = [], collidee, opoints = [];

	// IMPORTANT Iterate in the reverse order since items at the end of the
	// list have an higher weight/priority and thus should be less impacted
	// by the overlapping strategy.

	for (i = labels.length - 1; i >= 0; --i) {
		s0 = labels[i].$layout;
		if (!opoints[s0._idx]) {
			// opoints[s0._idx] = getBoxOuterPoints(s0)
			opoints[s0._idx] = convertToNamed(s0._box._points())
		}
		s0._opoints = opoints[s0._idx]
		for (j = i - 1; j >= 0 && s0._visible; --j) {
			s1 = labels[j].$layout;
			if (!opoints[s1._idx]) {
				// opoints[s1._idx] = getBoxOuterPoints(s1)
				opoints[s0._idx] = convertToNamed(s0._box._points())
			}
			s1._opoints = opoints[s1._idx]

			if (s1._visible && s0._box.intersects(s1._box)) {
				collidee = collider(s0, s1, labels)
				if (collidee)
					colliding.push(collidee);
			}
		}
	}

	handleCollision(labels, colliding)


	return { labels, colliding };
}

function defaultCollider(s0, s1) {
	console.log('default collider');
	var h0 = s0._hidable;
	var h1 = s1._hidable;

	if ((h0 && h1) || h1) {
		s1._visible = false;
	} else if (h0) {
		s0._visible = false;
	}
}

function getLabelBasedOnIdx(idx, labels) {
	const l =  labels.find(v =>  v._index == idx )
	const ix = labels.findIndex(v =>  v._index == idx )
	return { label: l, ix }
}

function anotherColliderFunc(s0, s1) {
	console.log(this);
	console.log('another collider', 's0', s0,  's1', s1)


	// do the calculations here
	return { s0, s1 }
}

function compute$1(labels) {
	var i, ilen, label, state, geometry, center, isFixable = false;

	// Initialize labels for overlap detection
	for (i = 0, ilen = labels.length; i < ilen; ++i) {
		label = labels[i];
		state = label.$layout;

		if (state._visible) {
			geometry = label.geometry();
			center = coordinates(label._el._model, label.model(), geometry);
			state._box.update(center, geometry, label.rotation());
		}
		isFixable = state._fixable;
	}

	console.log('compute$1')
	let colliderFunc = defaultCollider;

	if (isFixable) {
		colliderFunc = anotherColliderFunc;
	}
	// Auto hide overlapping labels
	return collide(labels, colliderFunc);
}

var layout = {
	prepare: function(datasets) {
		var labels = [];
		var i, j, ilen, jlen, label;




		for (i = 0, ilen = datasets.length; i < ilen; ++i) {
			for (j = 0, jlen = datasets[i].length; j < jlen; ++j) {

				label = datasets[i][j];
				labels.push(label);
				console.log('layout prepare - label', label.$context.perc)
				label.$layout = {
					_box: new HitBox(),
					_hidable: false,
					_visible: true,
					_set: i,
					_idx: j,
					_perc: label.$context.perc
				};

				console.log('layout prepare - label', label)
			}
		}

		// TODO New `z` option: labels with a higher z-index are drawn
		// of top of the ones with a lower index. Lowest z-index labels
		// are also discarded first when hiding overlapping labels.
		labels.sort(function(a, b) {
			var sa = a.$layout;
			var sb = b.$layout;

			return sa._idx === sb._idx
				? sb._set - sa._set
				: sb._idx - sa._idx;
		});

		this.update(labels);

		return labels;
	},

	update: function(labels) {
		var dirty = false;
		var fixLabelOverlap = false;
		var i, ilen, label, model, state;

		for (i = 0, ilen = labels.length; i < ilen; ++i) {
			label = labels[i];
			model = label.model();
			state = label.$layout;
			state._hidable = model && model.display === 'auto';
			state._fixable = model && model.fixLabelOverlap;
			state._visible = label.visible();

			dirty |= state._hidable;
			fixLabelOverlap |= state._fixable;
			console.log('fixLabelOverlap', fixLabelOverlap)

		}

		if (dirty || fixLabelOverlap) {

			const computed =  compute$1(labels);
			console.log('computed', computed);
			return computed;
		}
	},

	lookup: function(labels, point) {
		var i, state;

		// IMPORTANT Iterate in the reverse order since items at the end of
		// the list have an higher z-index, thus should be picked first.

		for (i = labels.length - 1; i >= 0; --i) {
			state = labels[i].$layout;

			if (state && state._visible && state._box.contains(point)) {
				return labels[i];
			}
		}

		return null;
	},

	draw: function(chart, labels) {
		var i, ilen, label, state, geometry, center;

		for (i = 0, ilen = labels.length; i < ilen; ++i) {
			label = labels[i];
			state = label.$layout;

			if (state._visible) {
				geometry = label.geometry();
				center = coordinates(label._el._view, label.model(), geometry);
				state._box.update(center, geometry, label.rotation());
				label.draw(chart, center);
			}
		}
	}
};

var helpers$3 = Chart.helpers;

var formatter = function(value) {
	if (helpers$3.isNullOrUndef(value)) {
		return null;
	}

	var label = value;
	var keys, klen, k;
	if (helpers$3.isObject(value)) {
		if (!helpers$3.isNullOrUndef(value.label)) {
			label = value.label;
		} else if (!helpers$3.isNullOrUndef(value.r)) {
			label = value.r;
		} else {
			label = '';
			keys = Object.keys(value);
			for (k = 0, klen = keys.length; k < klen; ++k) {
				label += (k !== 0 ? ', ' : '') + keys[k] + ': ' + value[keys[k]];
			}
		}
	}

	return '' + label;
};

/**
 * IMPORTANT: make sure to also update tests and TypeScript definition
 * files (`/test/specs/defaults.spec.js` and `/types/options.d.ts`)
 */

var defaults = {
	align: 'center',
	anchor: 'center',
	backgroundColor: null,
	borderColor: null,
	borderRadius: 0,
	borderWidth: 0,
	clamp: false,
	clip: false,
	color: undefined,
	display: true,
	font: {
		family: undefined,
		lineHeight: 1.2,
		size: undefined,
		style: undefined,
		weight: null
	},
	formatter: formatter,
	labels: undefined,
	listeners: {},
	offset: 4,
	opacity: 1,
	padding: {
		top: 4,
		right: 4,
		bottom: 4,
		left: 4
	},
	rotation: 0,
	textAlign: 'start',
	textStrokeColor: undefined,
	textStrokeWidth: 0,
	textShadowBlur: 0,
	textShadowColor: undefined,
	fixLabelOverlap: false
};

/**
 * @see https://github.com/chartjs/Chart.js/issues/4176
 */

var helpers$4 = Chart.helpers;
var EXPANDO_KEY = '$datalabels';
var DEFAULT_KEY = '$default';

function configure(dataset, options) {
	var override = dataset.datalabels;
	var listeners = {};
	var configs = [];
	var labels, keys;

	if (override === false) {
		return null;
	}
	if (override === true) {
		override = {};
	}

	options = helpers$4.merge({}, [options, override]);
	console.log('options', options	)
	labels = options.labels || {};
	keys = Object.keys(labels);
	delete options.labels;

	if (keys.length) {
		keys.forEach(function(key) {
			if (labels[key]) {
				configs.push(helpers$4.merge({}, [
					options,
					labels[key],
					{_key: key}
				]));
			}
		});
	} else {
		// Default label if no "named" label defined.
		configs.push(options);
	}

	// listeners: {<event-type>: {<label-key>: <fn>}}
	listeners = configs.reduce(function(target, config) {
		helpers$4.each(config.listeners || {}, function(fn, event) {
			target[event] = target[event] || {};
			target[event][config._key || DEFAULT_KEY] = fn;
		});

		delete config.listeners;
		return target;
	}, {});

	return {
		labels: configs,
		listeners: listeners
	};
}

function dispatchEvent(chart, listeners, label) {
	if (!listeners) {
		return;
	}

	var context = label.$context;
	var groups = label.$groups;
	var callback;

	if (!listeners[groups._set]) {
		return;
	}

	callback = listeners[groups._set][groups._key];
	if (!callback) {
		return;
	}

	if (helpers$4.callback(callback, [context]) === true) {
		// Users are allowed to tweak the given context by injecting values that can be
		// used in scriptable options to display labels differently based on the current
		// event (e.g. highlight an hovered label). That's why we update the label with
		// the output context and schedule a new chart render by setting it dirty.
		chart[EXPANDO_KEY]._dirty = true;
		label.update(context);
	}
}

function dispatchMoveEvents(chart, listeners, previous, label) {
	var enter, leave;

	if (!previous && !label) {
		return;
	}

	if (!previous) {
		enter = true;
	} else if (!label) {
		leave = true;
	} else if (previous !== label) {
		leave = enter = true;
	}

	if (leave) {
		dispatchEvent(chart, listeners.leave, previous);
	}
	if (enter) {
		dispatchEvent(chart, listeners.enter, label);
	}
}

function handleMoveEvents(chart, event) {
	var expando = chart[EXPANDO_KEY];
	var listeners = expando._listeners;
	var previous, label;

	if (!listeners.enter && !listeners.leave) {
		return;
	}

	if (event.type === 'mousemove') {
		label = layout.lookup(expando._labels, event);
	} else if (event.type !== 'mouseout') {
		return;
	}

	previous = expando._hovered;
	expando._hovered = label;
	dispatchMoveEvents(chart, listeners, previous, label);
}

function handleClickEvents(chart, event) {
	var expando = chart[EXPANDO_KEY];
	var handlers = expando._listeners.click;
	var label = handlers && layout.lookup(expando._labels, event);
	if (label) {
		dispatchEvent(chart, handlers, label);
	}
}

// https://github.com/chartjs/chartjs-plugin-datalabels/issues/108
function invalidate(chart) {
	if (chart.animating) {
		return;
	}

	// `chart.animating` can be `false` even if there is animation in progress,
	// so let's iterate all animations to find if there is one for the `chart`.
	var animations = Chart.animationService.animations;
	for (var i = 0, ilen = animations.length; i < ilen; ++i) {
		if (animations[i].chart === chart) {
			return;
		}
	}

	// No render scheduled: trigger a "lazy" render that can be canceled in case
	// of hover interactions. The 1ms duration is a workaround to make sure an
	// animation is created so the controller can stop it before any transition.
	chart.render({duration: 1, lazy: true});
}

Chart.defaults.global.plugins.datalabels = defaults;

var plugin = {
	id: 'datalabels',

	beforeInit: function(chart) {
		chart[EXPANDO_KEY] = {
			_actives: []
		};
	},

	beforeUpdate: function(chart) {
		var expando = chart[EXPANDO_KEY];
		expando._listened = false;
		expando._listeners = {};     // {<event-type>: {<dataset-index>: {<label-key>: <fn>}}}
		expando._datasets = [];      // per dataset labels: [Label[]]
		expando._labels = [];        // layouted labels: Label[]
	},

	afterDatasetUpdate: function(chart, args, options) {
		var datasetIndex = args.index;
		var expando = chart[EXPANDO_KEY];
		var labels = expando._datasets[datasetIndex] = [];
		var visible = chart.isDatasetVisible(datasetIndex);
		var dataset = chart.data.datasets[datasetIndex];
		console.log('dataset', dataset);
		var config = configure(dataset, options);
		var elements = args.meta.data || [];
		var ctx = chart.ctx;
		var i, j, ilen, jlen, cfg, key, el, label;

		ctx.save();

		const calcPercentage = function(value, dataArr, formatted = true) {
			let sum = 0;
			dataArr.map(data => {
				sum += data;
			});
			let percentage = (value * 100 / sum);
			if (formatted)
			  return percentage.toFixed(2) + "%";
			else
			  return percentage;
		}

		for (i = 0, ilen = elements.length; i < ilen; ++i) {
			el = elements[i];
			el[EXPANDO_KEY] = [];

			if (visible && el && !el.hidden && !el._model.skip) {
				for (j = 0, jlen = config.labels.length; j < jlen; ++j) {
					cfg = config.labels[j];
					key = cfg._key;

					label = new Label(cfg, ctx, el, i);

					label.$groups = {
						_set: datasetIndex,
						_key: key || DEFAULT_KEY
					};
					label.$context = {
						active: false,
						chart: chart,
						dataIndex: i,
						dataset: dataset,
						datasetIndex: datasetIndex,
						perc: calcPercentage(dataset.data[i], dataset.data, false)
					};
					console.log('label', label)
					// label.$layout._perc = perc

					label.update(label.$context);
					el[EXPANDO_KEY].push(label);
					labels.push(label);
				}
			}
		}

		ctx.restore();

		// Store listeners at the chart level and per event type to optimize
		// cases where no listeners are registered for a specific event.
		helpers$4.merge(expando._listeners, config.listeners, {
			merger: function(event, target, source) {
				target[event] = target[event] || {};
				target[event][args.index] = source[event];
				expando._listened = true;
			}
		});
	},

	afterUpdate: function(chart, options) {
		chart[EXPANDO_KEY]._labels = layout.prepare(
			chart[EXPANDO_KEY]._datasets,
			options);
	},

	// Draw labels on top of all dataset elements
	// https://github.com/chartjs/chartjs-plugin-datalabels/issues/29
	// https://github.com/chartjs/chartjs-plugin-datalabels/issues/32
	afterDatasetsDraw: function(chart) {
		layout.draw(chart, chart[EXPANDO_KEY]._labels);
	},

	beforeEvent: function(chart, event) {
		// If there is no listener registered for this chart, `listened` will be false,
		// meaning we can immediately ignore the incoming event and avoid useless extra
		// computation for users who don't implement label interactions.
		if (chart[EXPANDO_KEY]._listened) {
			switch (event.type) {
			case 'mousemove':
			case 'mouseout':
				handleMoveEvents(chart, event);
				break;
			case 'click':
				handleClickEvents(chart, event);
				break;
			default:
			}
		}
	},

	afterEvent: function(chart) {
		var expando = chart[EXPANDO_KEY];
		var previous = expando._actives;
		var actives = expando._actives = chart.lastActive || [];  // public API?!
		var updates = utils.arrayDiff(previous, actives);
		var i, ilen, j, jlen, update, label, labels;

		for (i = 0, ilen = updates.length; i < ilen; ++i) {
			update = updates[i];
			if (update[1]) {
				labels = update[0][EXPANDO_KEY] || [];
				for (j = 0, jlen = labels.length; j < jlen; ++j) {
					label = labels[j];
					label.$context.active = (update[1] === 1);
					label.update(label.$context);
				}
			}
		}

		if (expando._dirty || updates.length) {
			let computed = layout.update(expando._labels);

			if (computed) {
				console.log('computed', computed);
			}
			invalidate(chart);
		}

		delete expando._dirty;
	}
};

// TODO Remove at version 1, we shouldn't automatically register plugins.
// https://github.com/chartjs/chartjs-plugin-datalabels/issues/42
Chart.plugins.register(plugin);

return plugin;

}));
